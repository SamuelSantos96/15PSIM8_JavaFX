
public class Jogador_Jogar {
	
	//Atributos
	private String 	username,
					resultado;
	
	private int 	codJogo,
					codJogador;
	
	//Contrutores
	public Jogador_Jogar() {
		
	}
	
	public Jogador_Jogar(int codJogo, int codJogador, String username, String resultado) {
		this.codJogo = codJogo;
		this.codJogador = codJogador;
		this.username = username;
		this.resultado = resultado;
	}

	//Getters e Setters
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	public int getCodJogo() {
		return codJogo;
	}

	public void setCodJogo(int codJogo) {
		this.codJogo = codJogo;
	}

	public int getCodJogador() {
		return codJogador;
	}

	public void setCodJogador(int codJogador) {
		this.codJogador = codJogador;
	}
	
}
