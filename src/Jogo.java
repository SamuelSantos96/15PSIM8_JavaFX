

public class Jogo {
	
	//Atributos
	private String 	data;
	
	private int 	codJogo;
	
	private boolean carimbo = false;

	//Construtores
	public Jogo() {
		
	}
	
	public Jogo(int codJogo, String data, boolean carimbo) {
		this.codJogo = codJogo;
		this.data = data;
		this.carimbo = carimbo;
	}
	
	//Getters e Setters
	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public int getCodJogo() {
		return codJogo;
	}

	public void setCodJogo(int codJogo) {
		this.codJogo = codJogo;
	}

	public boolean isCarimbo() {
		return carimbo;
	}

	public void setCarimbo(boolean carimbo) {
		this.carimbo = carimbo;
	}
	
}
