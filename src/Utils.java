

import java.sql.Connection;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.PasswordField;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {
	
	static Label label1 = new Label("1"); 					//Cria a label para mostra
	static Label label2 = new Label("2"); 					//Cria a label para mostra
	static Label label3 = new Label("3"); 					//Cria a label para mostra
	static Label label4 = new Label("4"); 					//Cria a label para mostra
	static Label label5 = new Label("5"); 					//Cria a label para mostra
	static Label label6 = new Label("6"); 					//Cria a label para mostra
	static Label label7 = new Label("7"); 					//Cria a label para mostra
	static Label label8 = new Label("8"); 					//Cria a label para mostra
	static Label label9 = new Label("9"); 					//Cria a label para mostra
	
	static boolean resposta;								//Vari�vel de confirma��o a escolha do utilizador

	//static boolean recConta;								//Vari�vel de confirma��o da recupera��o de conta
	
	//static int codRecConta;									//Vari�vel de confirma��o da recupera��o de conta
	
	static boolean criacaoNegada;							//Verifica se a cria��o � ou n�o negada
	
	//static boolean recNegada;								//Verifica se a recupera��o � ou n�o negada
	
	static boolean alterarUtilizador = false;				//Verifica se a altera��o est� a decorrer
	
	static boolean anularUtilizador = false;				//Verifica se a anula��o est� a decorrer
	
	static boolean alterarParametro = false;				//Verifica se a altera��o est� a decorrer
	
	static boolean anularParametro = false;					//Verifica se a anula��o est� a decorrer
	
	static Jogador jogadorSelecionado = null;				//Recebe o jogador selecionado da lista, para altera��o/elimina��o
	
	//static Parametrizador parametrizadorSelecionado = null;	//Recebe o parametrizador selecionado da lista, para altera��o/elimina��o
	
	static Jogo jogoSelecionado = null;
	
	//static Parametro parametroSelecionado = null;
	
	//static Recuperacao recuperacaoSelecionado = null;
	
	static Connection conn = null;							// Liga��o a base de dados
	
	//static int recContaPassword;							//Guarda a password da conta da recupera��o da conta
	
	static int 	i;											//Contador
	
	static boolean 	loginJogador = false,					//Vari�vel de indica��o de login feito por jogador
					//loginAdministrador = false,				//Vari�vel de indica��o de login feito por administrador
					//loginParametrizador = false,			//Vari�vel de indica��o de login feito por parametrizador
					loginDoConvidado = false;				//Vari�vel de indica��o de login feito pelo convidado
					
	static Image image1 = new Image("file:images\\question-mark-dude.png");		//Cria uma Image
	static Image image2 = new Image("file:images\\question-mark-dude.png");		//Cria uma Image
	static Image image3 = new Image("file:images\\question-mark-dude.png");		//Cria uma Image
	static Image image4 = new Image("file:images\\question-mark-dude.png");		//Cria uma Image
	static Image image5 = new Image("file:images\\question-mark-dude.png");		//Cria uma Image
	static Image image6 = new Image("file:images\\question-mark-dude.png");		//Cria uma Image
	static Image image7 = new Image("file:images\\question-mark-dude.png");		//Cria uma Image
	static Image image8 = new Image("file:images\\question-mark-dude.png");		//Cria uma Image
	static Image image9 = new Image("file:images\\question-mark-dude.png");		//Cria uma Image
    
	static Image image1o = new Image("file:images\\tic-tac-toe-O.png");			//Cria uma Image
	static Image image2o = new Image("file:images\\tic-tac-toe-O.png");			//Cria uma Image
	static Image image3o = new Image("file:images\\tic-tac-toe-O.png");			//Cria uma Image
	static Image image4o = new Image("file:images\\tic-tac-toe-O.png");			//Cria uma Image
	static Image image5o = new Image("file:images\\tic-tac-toe-O.png");			//Cria uma Image
	static Image image6o = new Image("file:images\\tic-tac-toe-O.png");			//Cria uma Image
	static Image image7o = new Image("file:images\\tic-tac-toe-O.png");			//Cria uma Image
	static Image image8o = new Image("file:images\\tic-tac-toe-O.png");			//Cria uma Image
	static Image image9o = new Image("file:images\\tic-tac-toe-O.png");			//Cria uma Image	    
    
	static Image image1x = new Image("file:images\\tic-tac-toe-X.png");			//Cria uma Image
	static Image image2x = new Image("file:images\\tic-tac-toe-X.png");			//Cria uma Image
	static Image image3x = new Image("file:images\\tic-tac-toe-X.png");			//Cria uma Image
	static Image image4x = new Image("file:images\\tic-tac-toe-X.png");			//Cria uma Image
	static Image image5x = new Image("file:images\\tic-tac-toe-X.png");			//Cria uma Image
	static Image image6x = new Image("file:images\\tic-tac-toe-X.png");			//Cria uma Image
	static Image image7x = new Image("file:images\\tic-tac-toe-X.png");			//Cria uma Image
	static Image image8x = new Image("file:images\\tic-tac-toe-X.png");			//Cria uma Image
	static Image image9x = new Image("file:images\\tic-tac-toe-X.png");			//Cria uma Image
	
	static ImageView imgv1 = new ImageView(image1);						//Cria uma ImageView com a Image
	static ImageView imgv2 = new ImageView(image2);						//Cria uma ImageView com a Image
	static ImageView imgv3 = new ImageView(image3);						//Cria uma ImageView com a Image
	static ImageView imgv4 = new ImageView(image4);						//Cria uma ImageView com a Image
	static ImageView imgv5 = new ImageView(image5);						//Cria uma ImageView com a Image
	static ImageView imgv6 = new ImageView(image6);						//Cria uma ImageView com a Image
	static ImageView imgv7 = new ImageView(image7);						//Cria uma ImageView com a Image
	static ImageView imgv8 = new ImageView(image8);						//Cria uma ImageView com a Image
	static ImageView imgv9 = new ImageView(image9);						//Cria uma ImageView com a Image
	
	static ImageView imgv1o = new ImageView(image1o);						//Cria uma ImageView com a Image
	static ImageView imgv2o = new ImageView(image2o);						//Cria uma ImageView com a Image
	static ImageView imgv3o = new ImageView(image3o);						//Cria uma ImageView com a Image
	static ImageView imgv4o = new ImageView(image4o);						//Cria uma ImageView com a Image
	static ImageView imgv5o = new ImageView(image5o);						//Cria uma ImageView com a Image
	static ImageView imgv6o = new ImageView(image6o);						//Cria uma ImageView com a Image
	static ImageView imgv7o = new ImageView(image7o);						//Cria uma ImageView com a Image
	static ImageView imgv8o = new ImageView(image8o);						//Cria uma ImageView com a Image
	static ImageView imgv9o = new ImageView(image9o);						//Cria uma ImageView com a Image
	 	
	static ImageView imgv1x = new ImageView(image1x);						//Cria uma ImageView com a Image
	static ImageView imgv2x = new ImageView(image2x);						//Cria uma ImageView com a Image
	static ImageView imgv3x = new ImageView(image3x);						//Cria uma ImageView com a Image
	static ImageView imgv4x = new ImageView(image4x);						//Cria uma ImageView com a Image
	static ImageView imgv5x = new ImageView(image5x);						//Cria uma ImageView com a Image
	static ImageView imgv6x = new ImageView(image6x);						//Cria uma ImageView com a Image
	static ImageView imgv7x = new ImageView(image7x);						//Cria uma ImageView com a Image
	static ImageView imgv8x = new ImageView(image8x);						//Cria uma ImageView com a Image
	static ImageView imgv9x = new ImageView(image9x);						//Cria uma ImageView com a Image
	
	//Janela que apresenta o login para o utilizador convidado
	public static void loginDoConvidado(){ //Static para n�o ser instanciada
		
		loginDoConvidado = false;
				
		//Janela login
		Stage janelaLoginDoConvidado = new Stage();							//Cria uma window
		//janelaLogin.initModality(Modality.APPLICATION_MODAL);				//Define uma janelaLogin Modal
		janelaLoginDoConvidado.initModality(Modality.APPLICATION_MODAL);	//Define uma janelaLogin Modal
		janelaLoginDoConvidado.setTitle("Log In"); 							//Como t�tulo, recebe a string do parametro
		janelaLoginDoConvidado.setMinWidth(500);							//Largura da janelaLogin
		janelaLoginDoConvidado.setMaxWidth(500);
		janelaLoginDoConvidado.setMinHeight(300);
		janelaLoginDoConvidado.setMaxHeight(300);
		
		//layout 
		GridPane layout = new GridPane();
		//Labels
		Label labelUserName = new Label("Username: ");
		Label labelPassword = new Label("Password: ");
		//TextFields
		TextField textFieldUserName = new TextField();
		PasswordField passwordFieldPassword = new PasswordField();
		
		Button btnOk = new Button("OK");							//Cria bot�o para fazer login
		btnOk.setOnAction(e -> {
			//janelaLoginDoConvidado.hide();
			//janelaLogin.close();
			if(textFieldUserName.getText().equals("") || passwordFieldPassword.getText().equals("")) {
				alertBox("Erro", "Um ou mais campos nulos!");
				janelaLoginDoConvidado.show();
			}
			else
			{
				if(textFieldUserName.getText().equals(Globais.getUsernameUtilizadorAtivo())) {
					alertBox("Erro", "Utilizador n�o v�lido!");
				}
				else {					
					//Estrutura de controlo para verificar se o username e password est�o na lista de jogadores
					for(i = 0; i < UtilsSQLConn.carregaListaJogadores().size(); i++) {
						if(UtilsSQLConn.carregaListaJogadores().get(i).getUsername().equals(textFieldUserName.getText()) &&
						   UtilsSQLConn.carregaListaJogadores().get(i).getPassword().equals(passwordFieldPassword.getText())) {
							Globais.setCodUtilizadorConvidado(UtilsSQLConn.carregaListaJogadores().get(i).getCodJogador());
							Globais.setUsernameUtilizadorConvidado(UtilsSQLConn.carregaListaJogadores().get(i).getUsername());
							Globais.setPasswordUtilizadorConvidado(UtilsSQLConn.carregaListaJogadores().get(i).getPassword());
							loginDoConvidado = true;
						}
					}
									
					//Se o login do convidado foi feito
					if(loginDoConvidado == true) {						
						//tictactoe();
						janelaLoginDoConvidado.close();
					}
					//Se os dados n�o correspondem a nenhum dos indiv�duos acima referidos envia uma mensagem de erro
					else {					
						//janelaLoginDoConvidado.hide();
						alertBox("Erro", "Um ou mais dados incorretos!");
						janelaLoginDoConvidado.show();
					}
				}
			}
		});				//A��o fecha esta janelaLogin
		
		Button btnCriarConta = new Button("Criar Conta");		//Cria bot�o para criar conta de jogador
		btnCriarConta.setOnAction(e -> criarContaJogador());	//Aceder � janela de criar conta
		
		//Adicionar � Grid os botoes, label e textfields-----------------------------
		layout.add(labelUserName, 0, 0);
		layout.add(textFieldUserName, 1, 0);
		layout.add(labelPassword, 0, 2);
		layout.add(passwordFieldPassword, 1,2);
		layout.add(btnOk, 0, 3);
		layout.add(btnCriarConta, 1, 3);
		layout.setAlignment(Pos.CENTER);
		
		//SCENE
		Scene scene = new Scene(layout);					//Criar a Scene e associa o Layout
		janelaLoginDoConvidado.setScene(scene);								//Associa a Scena 
		janelaLoginDoConvidado.showAndWait();								//Executa e prende o controlo at� ser fechada
	}
	
	public static void login(){ //Static para n�o ser instanciada
		
		loginJogador = false;							//Vari�vel de indica��o de login feito por jogador
				
		//Janela login
		Stage janelaLogin = new Stage();							//Cria uma window
		//janelaLogin.initModality(Modality.APPLICATION_MODAL);		//Define uma janelaLogin Modal
		janelaLogin.initModality(Modality.APPLICATION_MODAL);		//Define uma janelaLogin Modal
		janelaLogin.setTitle("Log In"); 							//Como t�tulo, recebe a string do parametro
		janelaLogin.setMinWidth(500);								//Largura da janelaLogin
		janelaLogin.setMaxWidth(500);
		janelaLogin.setMinHeight(300);
		janelaLogin.setMaxHeight(300);
		
		//layout 
		GridPane layout = new GridPane();
		//Labels
		Label labelUserName = new Label("Username: ");
		Label labelPassword = new Label("Password: ");
		//TextFields
		TextField textFieldUserName = new TextField();
		PasswordField passwordFieldPassword = new PasswordField();
		
		Button btnOk = new Button("OK");							//Cria bot�o para fazer login
		btnOk.setOnAction(e -> {
			//janelaLogin.hide();
			//janelaLogin.close();
			if(textFieldUserName.getText().equals("") || passwordFieldPassword.getText().equals("")) {
				alertBox("Erro", "Um ou mais campos nulos!");
				janelaLogin.show();
			}
			else
			{
				//Estrutura de controlo para verificar se o username e password est�o na lista de jogadores
				for(i = 0; i < UtilsSQLConn.carregaListaJogadores().size(); i++) {
					if(UtilsSQLConn.carregaListaJogadores().get(i).getUsername().equals(textFieldUserName.getText()) &&
					   UtilsSQLConn.carregaListaJogadores().get(i).getPassword().equals(passwordFieldPassword.getText())) {
						loginJogador = true;
						Globais.setCodUtilizadorAtivo(UtilsSQLConn.carregaListaJogadores().get(i).getCodJogador());
						Globais.setUsernameUtilizadorAtivo(UtilsSQLConn.carregaListaJogadores().get(i).getUsername());
						Globais.setPasswordUtilizadorAtivo(UtilsSQLConn.carregaListaJogadores().get(i).getPassword());
					}
				}

				//Se o login foi feito por um jogador
				if(loginJogador == true) {
					menuJogador();								//Acede ao Menu Jogador
					janelaLogin.close();
				}
				//Se os dados n�o correspondem a nenhum dos indiv�duos acima referidos envia uma mensagem de erro
				else {					
					//janelaLogin.hide();
					alertBox("Erro", "Um ou mais dados incorretos!");
					janelaLogin.show();
				}				
			}
		});				//A��o fecha esta janelaLogin
		
		Button btnCriarConta = new Button("Criar Conta");		//Cria bot�o para criar conta de jogador
		btnCriarConta.setOnAction(e -> criarContaJogador());	//Aceder � janela de criar conta
		
		//Adicionar � Grid os botoes, label e textfields-----------------------------
		layout.add(labelUserName, 0, 0);
		layout.add(textFieldUserName, 1, 0);
		layout.add(labelPassword, 0, 2);
		layout.add(passwordFieldPassword, 1,2);
		layout.add(btnOk, 0, 3);
		layout.add(btnCriarConta, 1, 3);
		layout.setAlignment(Pos.CENTER);
		
		//SCENE
		Scene scene = new Scene(layout);						//Criar a Scene e associa o Layout
		janelaLogin.setScene(scene);							//Associa a Scena 
		janelaLogin.showAndWait();								//Executa e prende o controlo at� ser fechada
	}
	//ALERTBOX para a exercicio  05a ModalWindow_AlertBox
		
	public static void alertBox(String title, String msg){ //Static para n�o ser instanciada
		
		Stage janela = new Stage();							//Cria uma window
		//janela.initModality(Modality.APPLICATION_MODAL);	//Define uma janela Modal
		janela.initModality(Modality.WINDOW_MODAL);	//Define uma janela Modal
		janela.setTitle(title); 							//Como t�tulo, recebe a string do parametro
		janela.setMinWidth(200);							//Largura da janela
		
		Label mensagem = new Label(msg); 					//Cria a label para mostra
		Button btnClose = new Button("Fechar");				//Cria bot�o para fechar janela
		btnClose.setOnAction(e -> janela.close());			//A��o fecha esta janela
		
		VBox layout = new VBox(10);							//Layout vertical com 10px entre c�lulas
		layout.getChildren().addAll(mensagem, btnClose);	//Adiciona Label e Button ao layout
		layout.setAlignment(Pos.CENTER);					//Alinhar os cnteudos ao Centros
		
		Scene scene = new Scene(layout);					//Criar a Scene e associa o Layout
		janela.setScene(scene);								//Associa a Scena 
		janela.showAndWait();								//Executa e prende o controlo at� ser fechada
		
		
	}
		
	public static boolean confirmationBox(String title, String msg){ //Static para n�o ser instanciada
		
		Stage janela = new Stage();							//Cria uma window
		//janela.initModality(Modality.APPLICATION_MODAL);	//Define uma janela Modal
		janela.initModality(Modality.WINDOW_MODAL);	//Define uma janela Modal
		janela.setTitle(title); 							//Como t�tulo, recebe a string do parametro
		janela.setMinWidth(200);							//Largura da janela
		
		Label mensagem = new Label(msg); 					//Cria a label para mostra
		Button btnTrue = new Button("Sim");				//Cria bot�o para fechar janela
		btnTrue.setOnAction(e -> {
			resposta = true;
			janela.close();			//A��o fecha esta janela
		});
		Button btnFalse = new Button("N�o");
		btnFalse.setOnAction(e -> {
			resposta = false;
			janela.close();			//A��o fecha esta janela
		});
		
		VBox layout = new VBox(10);							//Layout vertical com 10px entre c�lulas
		VBox layout1 = new VBox(10);
		layout.getChildren().addAll(mensagem, layout1);
		layout.setAlignment(Pos.CENTER);
		
		layout1.getChildren().addAll(btnTrue, btnFalse);
	
		
		Scene scene = new Scene(layout);					//Criar a Scene e associa o Layout
		janela.setScene(scene);								//Associa a Scena 
		janela.showAndWait();								//Executa e prende o controlo at� ser fechada
		
		return resposta;
	}
	
	public static void janelaAlterarJogador(){ //Static para n�o ser instanciada
		
		Stage janela = new Stage();							//Cria uma window
		//janela.initModality(Modality.APPLICATION_MODAL);	//Define uma janela Modal
		janela.initModality(Modality.WINDOW_MODAL);	//Define uma janela Modal
		janela.setTitle("New"); 					//Como t�tulo, recebe a string do parametro
		janela.setMinWidth(200);							//Largura da janela
		
		//PasswordField
		Label password = new Label("Password: ");
		PasswordField passwordFieldPassword = new PasswordField();
		passwordFieldPassword.setPromptText("Password");
		//Bot�o Alterar
		Button btnAlterar = new Button("Alterar");
		btnAlterar.setOnAction(e -> {
			if(passwordFieldPassword.getText().equals("")) {
				alertBox("Erro", "Um ou mais campos nulos!");
			}
			else {
				/*
				ObservableList<Jogador> itemSelecionado = tabelaJogadores.getSelectionModel().getSelectedItems();
				jogadorSelecionado = itemSelecionado.get(0);
				// altera os dados para o nProc do item selecionado
				UtilsSQLConn.mySqlDml("Update Jogador set"
						+ " Username 	= '"+textFieldUserName.getText()+"', "			// aspas em num�ricos
						+ " Password 	= '"+passwordFieldPassword.getText()+"' "			// aspas e plicas em strings
						+ " where codJogador = "+jogadorSelecionado.getCodJogador()+" ");*/
				
				UtilsSQLConn.mySqlDml("Update Jogador set"
						+ " Password 	= '"+passwordFieldPassword.getText()+"' "			// aspas e plicas em strings
						+ " where codJogador = "+Globais.getCodUtilizadorAtivo()+" ");
				
				/*NOTAS:
				 * 1 - os campos alfab�ticos t�m que ser envolvidos em plicas
				 * 2 - os campos num�ricos n�o t�m.
				 * */
			}
			
			janela.close();
		});
		
		VBox layout = new VBox(10);							//Layout vertical com 10px entre c�lulas
		VBox layout1 = new VBox(10);
		layout.getChildren().addAll(layout1);
		layout.setAlignment(Pos.CENTER);
		
		layout1.getChildren().addAll(password, passwordFieldPassword, btnAlterar);
	
		
		Scene scene = new Scene(layout);					//Criar a Scene e associa o Layout
		janela.setScene(scene);								//Associa a Scena 
		janela.showAndWait();								//Executa e prende o controlo at� ser fechada
	}
	
	//Janela com o menu do jogador
	public static void menuJogador() {
		try {			
			//Janela do Jogador
			Stage janelaMenuJogador = new Stage();
			
			//Menus
			Menu menuJogar = new Menu("_Jogar");							//Menu Jogar
			Menu menuGerirConta = new Menu("Gerir _Conta");					//Menu Gerir Conta
			Menu menuListas = new Menu("_Listas");							//Menu Listas
			
			//Itens dos menus
			//MenuItem menu1Player = new MenuItem("1 Player");
			//menu1Player.setOnAction(e -> tictactoe());
			//menuJogar.getItems().add(menu1Player);
			MenuItem menu2Players = new MenuItem("2 Players");
			menu2Players.setOnAction(e -> {
				loginDoConvidado();
				
				if(loginDoConvidado == true) {
					tictactoe();
				}
			});
			menuJogar.getItems().add(menu2Players);

			MenuItem menuAlterarConta = new MenuItem("Alterar Conta");
			menuAlterarConta.setOnAction(e -> janelaAlterarJogador());
			menuGerirConta.getItems().add(menuAlterarConta);
			MenuItem menuAnularConta = new MenuItem("Anular Conta");
			menuAnularConta.setOnAction(e -> {
				UtilsSQLConn.mySqlDml("Delete from Jogador where codJogador = "+Globais.getCodUtilizadorAtivo()+" ");
				janelaMenuJogador.close();
				login();
			});
			menuGerirConta.getItems().add(menuAnularConta);
			
			MenuItem menuListaJogadores = new MenuItem("Jogadores");
			menuListaJogadores.setOnAction(e -> listaDeJogadores());
			menuListas.getItems().add(menuListaJogadores);
			
			MenuItem menuListaResultados = new MenuItem("Resultados");
			menuListaResultados.setOnAction(e -> listaDeResultados());
			menuListas.getItems().add(menuListaResultados);
			
			MenuItem menuListaJogos = new MenuItem("Jogos");
			menuListaJogos.setOnAction(e -> listaDeJogos());
			menuListas.getItems().add(menuListaJogos);
			
			//MenuBar com os menus associados
			MenuBar menuBar = new MenuBar();
			menuBar.getMenus().addAll(menuJogar, menuGerirConta, menuListas);
			
			//Associar o menu � regi�o TOP do rootLayout
			BorderPane rootLayout = new BorderPane();
			rootLayout.setTop(menuBar);
			
			//Layout da Janela
			GridPane grid = new GridPane();
		    grid.setHgap(10);
		    grid.setVgap(10);
		    grid.setPadding(new Insets(50, 50, 50, 50));

			//Username
			Label username = new Label("Username: ");
			grid.add(username, 0, 1);
			Label usernameJogador = new Label(Globais.getUsernameUtilizadorAtivo());
			grid.add(usernameJogador, 1, 1);

			//Bot�o para Sair
			Button btnSair = new Button("Log Out");
			btnSair.setOnAction(e -> {
				janelaMenuJogador.close();				
				login();
			});
			grid.add(btnSair, 0, 2);
			
			rootLayout.setCenter(grid);
			Scene scene = new Scene(rootLayout,500,400);
			janelaMenuJogador.setScene(scene);
			janelaMenuJogador.setTitle("Menu Jogador");
			janelaMenuJogador.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
	//Apresenta a janela com a lista de jogadores
	public static void listaDeJogadores() {
		try {
			//Janela de Jogadores
			Stage janelaListaJogadores = new Stage();
			
			//Tabela dos Jogadores
			TableView<Jogador> tabelaJogadores = new TableView<>();
			
			//Coluna C�digo do Jogador
			TableColumn<Jogador, String> colunaCodJogador = new TableColumn<>("C�digo");						
			colunaCodJogador.setMinWidth(200);	//Largura em pixeis da coluna
			colunaCodJogador.setCellValueFactory(new PropertyValueFactory<>("codJogador"));
			
			//Coluna Username
			TableColumn<Jogador, String> colunaUsername = new TableColumn<>("Username");						
			colunaUsername.setMinWidth(200);	//Largura em pixeis da coluna
			colunaUsername.setCellValueFactory(new PropertyValueFactory<>("username"));
			//Nome do atributo, na ObservableList, onde vai ler os dados
			
			//Coluna Password
			TableColumn<Jogador, String> colunaPassword = new TableColumn<>("Password");
			colunaPassword.setMinWidth(200);
			colunaPassword.setCellValueFactory(new PropertyValueFactory<>("password"));
			
			//Coluna Vit�rias
			TableColumn<Jogador, String> colunaVitorias = new TableColumn<>("Vit�rias");						
			colunaVitorias.setMinWidth(200);	//Largura em pixeis da coluna
			colunaVitorias.setCellValueFactory(new PropertyValueFactory<>("numVitorias"));
			
			//Coluna Derrotas
			TableColumn<Jogador, String> colunaDerrotas = new TableColumn<>("Derrotas");						
			colunaDerrotas.setMinWidth(200);	//Largura em pixeis da coluna
			colunaDerrotas.setCellValueFactory(new PropertyValueFactory<>("numDerrotas"));
			//Nome do atributo, na ObservableList, onde vai ler os dados
			
			//Coluna Empates
			TableColumn<Jogador, String> colunaEmpates = new TableColumn<>("Empates");
			colunaEmpates.setMinWidth(200);
			colunaEmpates.setCellValueFactory(new PropertyValueFactory<>("numEmpates"));
						
			
			
			//Associar as colunas � tabela
			//Se o utilizador for jogador apresenta a lista de jogadores sem passwords
			tabelaJogadores.getColumns().addAll(
					colunaCodJogador,
					colunaUsername, 
					colunaVitorias,
					colunaDerrotas,
					colunaEmpates);
						
			//Carregar a lista com dados
			tabelaJogadores.setItems( UtilsSQLConn.carregaListaJogadores() );
			
			//Prepara��o da janela
			StackPane layout = new StackPane();
			layout.setPadding(new Insets(20, 20, 20, 20));
			
			HBox layoutEdit = new HBox(10);
			layoutEdit.setPadding(new Insets(10, 10, 10, 10));
			//layoutEdit.setAlignment(Pos.BOTTOM_RIGHT);
			
			VBox layoutSub = new VBox(10);
			//layoutSub.getChildren().addAll(tabelaJogadores, layoutEdit);
			/*			
			if(alterarUtilizador == true) {
				layoutEdit.getChildren().addAll(textFieldUserName, passwordFieldPassword, btnAlterar);
				layoutSub.getChildren().addAll(tabelaJogadores, layoutEdit);
			}
			else if(anularUtilizador == true) {
				layoutEdit.getChildren().addAll(btnApagar);
				layoutSub.getChildren().addAll(tabelaJogadores, layoutEdit);
			}
			else {
				layoutSub.getChildren().add(tabelaJogadores);
			}*/
			
			layoutSub.getChildren().add(tabelaJogadores);
			
			//Scene scene = new Scene(layout, 400, 400);
			layout.getChildren().add(layoutSub);
			Scene scene = new Scene(layout);
			janelaListaJogadores.setScene(scene);
			janelaListaJogadores.setTitle("Lista de Jogadores");
			janelaListaJogadores.show();
			
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	//Apresenta a janela com a lista de jogador_jogar
	public static void listaDeResultados() {
		try {
			//Janela Jogador__Jogar
			Stage janelaJogador_Jogar = new Stage();
			
			//Tabela Jogador_Jogar
			TableView<Jogador_Jogar> tabelaJogador_Jogar = new TableView<>();
			
			//Coluna C�digo do Jogo
			TableColumn<Jogador_Jogar, String> colunaCodJogo = new TableColumn<>("C�digo Jogo");						
			colunaCodJogo.setMinWidth(200);	//Largura em pixeis da coluna
			colunaCodJogo.setCellValueFactory(new PropertyValueFactory<>("codJogo"));
			
			//Coluna C�digo do Jogador
			TableColumn<Jogador_Jogar, String> colunaCodJogador = new TableColumn<>("C�digo Jogador");						
			colunaCodJogador.setMinWidth(200);	//Largura em pixeis da coluna
			colunaCodJogador.setCellValueFactory(new PropertyValueFactory<>("codJogador"));
			//Nome do atributo, na ObservableList, onde vai ler os dados
			
			//Coluna Username
			TableColumn<Jogador_Jogar, String> colunaUsername = new TableColumn<>("Username");
			colunaUsername.setMinWidth(200);
			colunaUsername.setCellValueFactory(new PropertyValueFactory<>("username"));
			
			//Coluna Resultado
			TableColumn<Jogador_Jogar, String> colunaResultado = new TableColumn<>("Resultado");						
			colunaResultado.setMinWidth(200);	//Largura em pixeis da coluna
			colunaResultado.setCellValueFactory(new PropertyValueFactory<>("resultado"));
			
			//Associar as colunas � tabela
			tabelaJogador_Jogar.getColumns().addAll(
					colunaCodJogo,
					colunaCodJogador,
					colunaUsername,
					colunaResultado);
			
			
			//Carregar a lista com dados
			tabelaJogador_Jogar.setItems( UtilsSQLConn.carregaListaJogador_Jogar() );
			
			//Prepara��o da janela
			StackPane layout = new StackPane();
			layout.setPadding(new Insets(20, 20, 20, 20));
			layout.getChildren().add(tabelaJogador_Jogar);
			
			//Scene scene = new Scene(layout, 400, 400);
			Scene scene = new Scene(layout);
			janelaJogador_Jogar.setScene(scene);
			janelaJogador_Jogar.setTitle("Lista de Resultados");
			janelaJogador_Jogar.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	//Apresenta a janela com a lista de jogos
	public static void listaDeJogos() {
		try {
			//Janela de Jogadores
			Stage janelaListaJogadores = new Stage();
			
			//Tabela dos Jogadores
			TableView<Jogo> tabelaJogos = new TableView<>();
			
			//Coluna C�digo do Jogo
			TableColumn<Jogo, String> colunaCodJogo = new TableColumn<>("Jogo");						
			colunaCodJogo.setMinWidth(200);	//Largura em pixeis da coluna
			colunaCodJogo.setCellValueFactory(new PropertyValueFactory<>("codJogo"));
						
			//Coluna Username1
			TableColumn<Jogo, String> colunaData = new TableColumn<>("Data");						
			colunaData.setMinWidth(200);	//Largura em pixeis da coluna
			colunaData.setCellValueFactory(new PropertyValueFactory<>("data"));
			//Nome do atributo, na ObservableList, onde vai ler os dados
								
			//Bot�o Apagar
			Button btnApagar = new Button("Apagar");
			btnApagar.setOnAction(e -> {
				//Vamos apanhar o item selecionado e compara-lo com a lista de Alunos
				/*
				ObservableList<Jogador> jogadorselected, listaJogadores;
				listaJogadores = tabelaJogadores.getItems();
				jogadorselected = tabelaJogadores.getSelectionModel().getSelectedItems();
				jogadorselected.forEach(listaJogadores::remove);
				*/
				
				ObservableList<Jogo> itemSelecionado = tabelaJogos.getSelectionModel().getSelectedItems();
				jogoSelecionado = itemSelecionado.get(0);
				UtilsSQLConn.mySqlDml("Delete from Jogar where codJogo = "+jogoSelecionado.getCodJogo()+" ");
			});
			/*
			//Associar as colunas � tabela
			//Se o utilizador for jogador apresenta a lista de jogadores sem passwords
			if(loginJogador == true || loginParametrizador == true) {
				tabelaJogos.getColumns().addAll(colunaCodJogo, colunaCodJogador1, colunaUsername1, colunaCodJogador2, colunaUsername2, colunaResultado);
			}
			//Se o utilizador for administrador apresenta a lista de jogadores com passwords
			else {
				tabelaJogos.getColumns().addAll(colunaCodJogo, colunaCodJogador1, colunaUsername1, colunaPassword1, colunaCodJogador2, colunaUsername2, colunaPassword2, colunaResultado);
			}
			*/
			
			tabelaJogos.getColumns().addAll(
					colunaCodJogo,
					colunaData);
			
			//Carregar a lista com dados
			tabelaJogos.setItems( UtilsSQLConn.carregaListaJogos() );
			
			//Prepara��o da janela
			StackPane layout = new StackPane();
			layout.setPadding(new Insets(20, 20, 20, 20));
			
			HBox layoutEdit = new HBox(10);
			layoutEdit.setPadding(new Insets(10, 10, 10, 10));
			//layoutEdit.setAlignment(Pos.BOTTOM_RIGHT);
			
			VBox layoutSub = new VBox(10);
			//layoutSub.getChildren().addAll(tabelaJogadores, layoutEdit);
						
			if(alterarUtilizador == true) {
				//layoutEdit.getChildren().addAll(btnAlterar);
				//layoutSub.getChildren().addAll(tabelaJogos, layoutEdit);
				layoutSub.getChildren().add(tabelaJogos);
			}
			else if(anularUtilizador == true) {
				layoutEdit.getChildren().addAll(btnApagar);
				layoutSub.getChildren().addAll(tabelaJogos, layoutEdit);
			}
			else {
				layoutSub.getChildren().add(tabelaJogos);
			}
						
			//Scene scene = new Scene(layout, 400, 400);
			layout.getChildren().add(layoutSub);
			Scene scene = new Scene(layout);
			janelaListaJogadores.setScene(scene);
			janelaListaJogadores.setTitle("Lista de Jogos");
			janelaListaJogadores.show();
			/*
			//Prepara��o da janela
			StackPane layout = new StackPane();
			layout.setPadding(new Insets(20, 20, 20, 20));
			layout.getChildren().add(tabelaJogos);
			
			//Scene scene = new Scene(layout, 700, 400);
			Scene scene = new Scene(layout);
			janelaListaJogadores.setScene(scene);
			janelaListaJogadores.setTitle("Lista de Jogos");
			janelaListaJogadores.show();*/
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
		
	
	
	//Jogo
	public static void tictactoe() {
		
		Stage janelaTicTacToe = new Stage();							//Cria uma window
		//janela.initModality(Modality.APPLICATION_MODAL);				//Define uma janela Modal
		janelaTicTacToe.initModality(Modality.WINDOW_MODAL);			//Define uma janela Modal
		janelaTicTacToe.setTitle("Tic Tac Toe"); 						//Como t�tulo, recebe a string do parametro
		janelaTicTacToe.setMinWidth(600);								//Largura da janela
		janelaTicTacToe.setMinHeight(600);								//Altura da janela
	    
		imgv1.setFitWidth(100);											//Resizes the image
	    imgv1.setFitHeight(100);										//Resizes the image
	    imgv1.setPreserveRatio(true);									//Preserva o racio original da imagem
	    imgv1.setSmooth(true); 											//Filtro de alta qualidade
	    imgv1.setCache(true);											//P�e em cach
	    
	    imgv2.setFitWidth(100);											//Resizes the image
	    imgv2.setFitHeight(100);										//Resizes the image
	    imgv2.setPreserveRatio(true);									//Preserva o racio original da imagem
	    imgv2.setSmooth(true); 											//Filtro de alta qualidade
	    imgv2.setCache(true);											//P�e em cach
	    
	    imgv3.setFitWidth(100);											//Resizes the image
	    imgv3.setFitHeight(100);										//Resizes the image
	    imgv3.setPreserveRatio(true);									//Preserva o racio original da imagem
	    imgv3.setSmooth(true); 											//Filtro de alta qualidade
	    imgv3.setCache(true);											//P�e em cach
	    
	    imgv4.setFitWidth(100);											//Resizes the image
	    imgv4.setFitHeight(100);										//Resizes the image
	    imgv4.setPreserveRatio(true);									//Preserva o racio original da imagem
	    imgv4.setSmooth(true); 											//Filtro de alta qualidade
	    imgv4.setCache(true);											//P�e em cach
	    
	    imgv5.setFitWidth(100);											//Resizes the image
	    imgv5.setFitHeight(100);										//Resizes the image
	    imgv5.setPreserveRatio(true);									//Preserva o racio original da imagem
	    imgv5.setSmooth(true); 											//Filtro de alta qualidade
	    imgv5.setCache(true);											//P�e em cach
	    
	    imgv6.setFitWidth(100);											//Resizes the image
	    imgv6.setFitHeight(100);										//Resizes the image
	    imgv6.setPreserveRatio(true);									//Preserva o racio original da imagem
	    imgv6.setSmooth(true); 											//Filtro de alta qualidade
	    imgv6.setCache(true);											//P�e em cach
	    
	    imgv7.setFitWidth(100);											//Resizes the image
	    imgv7.setFitHeight(100);										//Resizes the image
	    imgv7.setPreserveRatio(true);									//Preserva o racio original da imagem
	    imgv7.setSmooth(true); 											//Filtro de alta qualidade
	    imgv7.setCache(true);											//P�e em cach
	    
	    imgv8.setFitWidth(100);											//Resizes the image
	    imgv8.setFitHeight(100);										//Resizes the image
	    imgv8.setPreserveRatio(true);									//Preserva o racio original da imagem
	    imgv8.setSmooth(true); 											//Filtro de alta qualidade
	    imgv8.setCache(true);											//P�e em cach
	    
	    imgv9.setFitWidth(100);											//Resizes the image
	    imgv9.setFitHeight(100);										//Resizes the image
	    imgv9.setPreserveRatio(true);									//Preserva o racio original da imagem
	    imgv9.setSmooth(true); 											//Filtro de alta qualidade
	    imgv9.setCache(true);											//P�e em cach
	    
	    
	    imgv1o.setFitWidth(100);										//Resizes the image
	    imgv1o.setFitHeight(100);										//Resizes the image
	    imgv1o.setPreserveRatio(true);									//Preserva o racio original da imagem
	    imgv1o.setSmooth(true); 										//Filtro de alta qualidade
	    imgv1o.setCache(true);											//P�e em cach
	    
	    imgv2o.setFitWidth(100);										//Resizes the image
	    imgv2o.setFitHeight(100);										//Resizes the image
	    imgv2o.setPreserveRatio(true);									//Preserva o racio original da imagem
	    imgv2o.setSmooth(true); 										//Filtro de alta qualidade
	    imgv2o.setCache(true);											//P�e em cach
	    
	    imgv3o.setFitWidth(100);										//Resizes the image
	    imgv3o.setFitHeight(100);										//Resizes the image
	    imgv3o.setPreserveRatio(true);									//Preserva o racio original da imagem
	    imgv3o.setSmooth(true); 										//Filtro de alta qualidade
	    imgv3o.setCache(true);											//P�e em cach
	    
	    imgv4o.setFitWidth(100);										//Resizes the image
	    imgv4o.setFitHeight(100);										//Resizes the image
	    imgv4o.setPreserveRatio(true);									//Preserva o racio original da imagem
	    imgv4o.setSmooth(true); 										//Filtro de alta qualidade
	    imgv4o.setCache(true);											//P�e em cach
	    
	    imgv5o.setFitWidth(100);										//Resizes the image
	    imgv5o.setFitHeight(100);										//Resizes the image
	    imgv5o.setPreserveRatio(true);									//Preserva o racio original da imagem
	    imgv5o.setSmooth(true); 										//Filtro de alta qualidade
	    imgv5o.setCache(true);											//P�e em cach
	    
	    imgv6o.setFitWidth(100);										//Resizes the image
	    imgv6o.setFitHeight(100);										//Resizes the image
	    imgv6o.setPreserveRatio(true);									//Preserva o racio original da imagem
	    imgv6o.setSmooth(true); 										//Filtro de alta qualidade
	    imgv6o.setCache(true);											//P�e em cach
	    
	    imgv7o.setFitWidth(100);										//Resizes the image
	    imgv7o.setFitHeight(100);										//Resizes the image
	    imgv7o.setPreserveRatio(true);									//Preserva o racio original da imagem
	    imgv7o.setSmooth(true); 										//Filtro de alta qualidade
	    imgv7o.setCache(true);											//P�e em cach
	    
	    imgv8o.setFitWidth(100);										//Resizes the image
	    imgv8o.setFitHeight(100);										//Resizes the image
	    imgv8o.setPreserveRatio(true);									//Preserva o racio original da imagem
	    imgv8o.setSmooth(true); 										//Filtro de alta qualidade
	    imgv8o.setCache(true);											//P�e em cach
	    
	    imgv9o.setFitWidth(100);										//Resizes the image
	    imgv9o.setFitHeight(100);										//Resizes the image
	    imgv9o.setPreserveRatio(true);									//Preserva o racio original da imagem
	    imgv9o.setSmooth(true); 										//Filtro de alta qualidade
	    imgv9o.setCache(true);											//P�e em cach
	    
	    
	    imgv1x.setFitWidth(100);										//Resizes the image
	    imgv1x.setFitHeight(100);										//Resizes the image
	    imgv1x.setPreserveRatio(true);									//Preserva o racio original da imagem
	    imgv1x.setSmooth(true); 										//Filtro de alta qualidade
	    imgv1x.setCache(true);											//P�e em cach
	    
	    imgv2x.setFitWidth(100);										//Resizes the image
	    imgv2x.setFitHeight(100);										//Resizes the image
	    imgv2x.setPreserveRatio(true);									//Preserva o racio original da imagem
	    imgv2x.setSmooth(true); 										//Filtro de alta qualidade
	    imgv2x.setCache(true);											//P�e em cach
	    
	    imgv3x.setFitWidth(100);										//Resizes the image
	    imgv3x.setFitHeight(100);										//Resizes the image
	    imgv3x.setPreserveRatio(true);									//Preserva o racio original da imagem
	    imgv3x.setSmooth(true); 										//Filtro de alta qualidade
	    imgv3x.setCache(true);											//P�e em cach
	    
	    imgv4x.setFitWidth(100);										//Resizes the image
	    imgv4x.setFitHeight(100);										//Resizes the image
	    imgv4x.setPreserveRatio(true);									//Preserva o racio original da imagem
	    imgv4x.setSmooth(true); 										//Filtro de alta qualidade
	    imgv4x.setCache(true);											//P�e em cach
	    
	    imgv5x.setFitWidth(100);										//Resizes the image
	    imgv5x.setFitHeight(100);										//Resizes the image
	    imgv5x.setPreserveRatio(true);									//Preserva o racio original da imagem
	    imgv5x.setSmooth(true); 										//Filtro de alta qualidade
	    imgv5x.setCache(true);											//P�e em cach
	    
	    imgv6x.setFitWidth(100);										//Resizes the image
	    imgv6x.setFitHeight(100);										//Resizes the image
	    imgv6x.setPreserveRatio(true);									//Preserva o racio original da imagem
	    imgv6x.setSmooth(true); 										//Filtro de alta qualidade
	    imgv6x.setCache(true);											//P�e em cach
	    
	    imgv7x.setFitWidth(100);										//Resizes the image
	    imgv7x.setFitHeight(100);										//Resizes the image
	    imgv7x.setPreserveRatio(true);									//Preserva o racio original da imagem
	    imgv7x.setSmooth(true); 										//Filtro de alta qualidade
	    imgv7x.setCache(true);											//P�e em cach
	    
	    imgv8x.setFitWidth(100);										//Resizes the image
	    imgv8x.setFitHeight(100);										//Resizes the image
	    imgv8x.setPreserveRatio(true);									//Preserva o racio original da imagem
	    imgv8x.setSmooth(true); 										//Filtro de alta qualidade
	    imgv8x.setCache(true);											//P�e em cach
	    
	    imgv9x.setFitWidth(100);										//Resizes the image
	    imgv9x.setFitHeight(100);										//Resizes the image
	    imgv9x.setPreserveRatio(true);									//Preserva o racio original da imagem
	    imgv9x.setSmooth(true); 										//Filtro de alta qualidade
	    imgv9x.setCache(true);											//P�e em cach
	    
		
		label1.setMinWidth(100);										//Define uma largura m�nima do label
		label1.setMinHeight(100);										//Define uma altura m�nima do label
		label1.setMaxWidth(100);										//Define uma largura m�xima do label
		label1.setMaxHeight(100);										//Define uma altura m�xima do label		
		label1.setGraphic(imgv1);										//Adiciona a ImageView ao Label
		label1.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
	        public void handle(MouseEvent arg0) {	            
				if(label1.getGraphic() == imgv1) {
					if(Globais.getJogadorAtivo() % 2 == 0) {
						label1.setGraphic(imgv1x);
					}
					else {
						label1.setGraphic(imgv1o);
					}
					
					confirmarVencedor();
					
					if(Globais.getVencedor() == 0) {
						Globais.setJogadorAtivo(Globais.getJogadorAtivo() + 1);
					}
					else {
						janelaTicTacToe.close();
						registarJogo();
					}
				}
			}
		});
		
		label2.setMinWidth(100);										//Define uma largura m�nima do label
		label2.setMinHeight(100);										//Define uma altura m�nima do label
		label2.setMaxWidth(100);										//Define uma largura m�xima do label
		label2.setMaxHeight(100);										//Define uma altura m�xima do label		
		label2.setGraphic(imgv2);											//Adiciona a ImageView ao Label
		label2.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
	        public void handle(MouseEvent arg0) {	            
				if(label2.getGraphic() == imgv2) {
					if(Globais.getJogadorAtivo() % 2 == 0) {
						label2.setGraphic(imgv2x);
					}
					else {
						label2.setGraphic(imgv2o);
					}
					
					confirmarVencedor();
					
					if(Globais.getVencedor() == 0) {
						Globais.setJogadorAtivo(Globais.getJogadorAtivo() + 1);
					}
					else {
						janelaTicTacToe.close();
						registarJogo();
					}
				}
			}
		});
		
		label3.setMinWidth(100);										//Define uma largura m�nima do label
		label3.setMinHeight(100);										//Define uma altura m�nima do label
		label3.setMaxWidth(100);										//Define uma largura m�xima do label
		label3.setMaxHeight(100);										//Define uma altura m�xima do label		
		label3.setGraphic(imgv3);										//Adiciona a ImageView ao Label
		label3.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
	        public void handle(MouseEvent arg0) {	            
				if(label3.getGraphic() == imgv3) {
					if(Globais.getJogadorAtivo() % 2 == 0) {
						label3.setGraphic(imgv3x);
					}
					else {
						label3.setGraphic(imgv3o);
					}
					
					confirmarVencedor();
					
					if(Globais.getVencedor() == 0) {
						Globais.setJogadorAtivo(Globais.getJogadorAtivo() + 1);
					}
					else {
						janelaTicTacToe.close();
						registarJogo();
					}
				}
			}
		});
		
		label4.setMinWidth(100);										//Define uma largura m�nima do label
		label4.setMinHeight(100);										//Define uma altura m�nima do label
		label4.setMaxWidth(100);										//Define uma largura m�xima do label
		label4.setMaxHeight(100);										//Define uma altura m�xima do label
		label4.setGraphic(imgv4);										//Adiciona a ImageView ao Label
		label4.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
	        public void handle(MouseEvent arg0) {	            
				if(label4.getGraphic() == imgv4) {
					if(Globais.getJogadorAtivo() % 2 == 0) {
						label4.setGraphic(imgv4x);
					}
					else {
						label4.setGraphic(imgv4o);
					}
					
					confirmarVencedor();
					
					if(Globais.getVencedor() == 0) {
						Globais.setJogadorAtivo(Globais.getJogadorAtivo() + 1);
					}
					else {
						janelaTicTacToe.close();
						registarJogo();
					}
				}
			}
		});
		
		label5.setMinWidth(100);										//Define uma largura m�nima do label
		label5.setMinHeight(100);										//Define uma altura m�nima do label
		label5.setMaxWidth(100);										//Define uma largura m�xima do label
		label5.setMaxHeight(100);										//Define uma altura m�xima do label
		label5.setGraphic(imgv5);										//Adiciona a ImageView ao Label
		label5.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
	        public void handle(MouseEvent arg0) {	            
				if(label5.getGraphic() == imgv5) {
					if(Globais.getJogadorAtivo() % 2 == 0) {
						label5.setGraphic(imgv5x);
					}
					else {
						label5.setGraphic(imgv5o);
					}
					
					confirmarVencedor();
					
					if(Globais.getVencedor() == 0) {
						Globais.setJogadorAtivo(Globais.getJogadorAtivo() + 1);
					}
					else {
						janelaTicTacToe.close();
						registarJogo();
					}
				}
			}
		});
		
		label6.setMinWidth(100);										//Define uma largura m�nima do label
		label6.setMinHeight(100);										//Define uma altura m�nima do label
		label6.setMaxWidth(100);										//Define uma largura m�xima do label
		label6.setMaxHeight(100);										//Define uma altura m�xima do label
		label6.setGraphic(imgv6);										//Adiciona a ImageView ao Label
		label6.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
	        public void handle(MouseEvent arg0) {
				if(label6.getGraphic() == imgv6) {
					if(Globais.getJogadorAtivo() % 2 == 0) {
						label6.setGraphic(imgv6x);
					}
					else {
						label6.setGraphic(imgv6o);
					}
					
					confirmarVencedor();
					
					if(Globais.getVencedor() == 0) {
						Globais.setJogadorAtivo(Globais.getJogadorAtivo() + 1);
					}
					else {
						janelaTicTacToe.close();
						registarJogo();
					}
				}
			}
		});
		
		label7.setMinWidth(100);										//Define uma largura m�nima do label
		label7.setMinHeight(100);										//Define uma altura m�nima do label
		label7.setMaxWidth(100);										//Define uma largura m�xima do label
		label7.setMaxHeight(100);										//Define uma altura m�xima do label
		label7.setGraphic(imgv7);										//Adiciona a ImageView ao Label
		label7.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
	        public void handle(MouseEvent arg0) {	            
				if(label7.getGraphic() == imgv7) {
					if(Globais.getJogadorAtivo() % 2 == 0) {
						label7.setGraphic(imgv7x);
					}
					else {
						label7.setGraphic(imgv7o);
					}
					
					confirmarVencedor();
					
					if(Globais.getVencedor() == 0) {
						Globais.setJogadorAtivo(Globais.getJogadorAtivo() + 1);
					}
					else {
						janelaTicTacToe.close();
						registarJogo();
					}
				}
			}
		});
		
		label8.setMinWidth(100);										//Define uma largura m�nima do label
		label8.setMinHeight(100);										//Define uma altura m�nima do label
		label8.setMaxWidth(100);										//Define uma largura m�xima do label
		label8.setMaxHeight(100);										//Define uma altura m�xima do label
		label8.setGraphic(imgv8);										//Adiciona a ImageView ao Label
		label8.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
	        public void handle(MouseEvent arg0) {	            
				if(label8.getGraphic() == imgv8) {
					if(Globais.getJogadorAtivo() % 2 == 0) {
						label8.setGraphic(imgv8x);
					}
					else {
						label8.setGraphic(imgv8o);
					}
					
					confirmarVencedor();
					
					if(Globais.getVencedor() == 0) {
						Globais.setJogadorAtivo(Globais.getJogadorAtivo() + 1);
					}
					else {
						janelaTicTacToe.close();
						registarJogo();
					}
				}
			}
		});
		
		label9.setMinWidth(100);										//Define uma largura m�nima do label
		label9.setMinHeight(100);										//Define uma altura m�nima do label
		label9.setMaxWidth(100);										//Define uma largura m�xima do label
		label9.setMaxHeight(100);										//Define uma altura m�xima do label
		label9.setGraphic(imgv9);										//Adiciona a ImageView ao Label
		label9.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
	        public void handle(MouseEvent arg0) {
				if(label9.getGraphic() == imgv9) {
					if(Globais.getJogadorAtivo() % 2 == 0) {
						label9.setGraphic(imgv9x);
					}
					else {
						label9.setGraphic(imgv9o);
					}
					
					confirmarVencedor();
					
					if(Globais.getVencedor() == 0) {
						Globais.setJogadorAtivo(Globais.getJogadorAtivo() + 1);
					}
					else {
						janelaTicTacToe.close();
						registarJogo();						
					}
				}
			}
		});
		//Button btnClose = new Button("Fechar");						//Cria bot�o para fechar janela
		//btnClose.setOnAction(e -> janelaTicTacToe.close());			//A��o fecha esta janela
		
		//GridPane grid;
		//VBox layout = new VBox(10);									//Layout vertical com 10px entre c�lulas
		
		//Cria o layout do tipo grid e adiciona os labels ao layout
		GridPane layout = new GridPane();
		layout.add(label1, 0, 0);
		layout.add(label2, 1, 0);
		layout.add(label3, 2, 0);
		layout.add(label4, 0, 1);
		layout.add(label5, 1, 1);
		layout.add(label6, 2, 1);
		layout.add(label7, 0, 2);
		layout.add(label8, 1, 2);
		layout.add(label9, 2, 2);
		
		//layout.getChildren().addAll(mensagem, btnClose);				//Adiciona Label e Button ao layout
		/*layout.getChildren().addAll(
				mensagem1, 
				mensagem2, 
				mensagem3, 
				mensagem4, 
				mensagem5, 
				mensagem6, 
				mensagem7, 
				mensagem8, 
				mensagem9);*/											//Adiciona Label e Button ao layout
		layout.setAlignment(Pos.CENTER);								//Alinhar os conteudos ao Centros
		
		VBox layoutPlayers = new VBox();
		layoutPlayers.setAlignment(Pos.TOP_CENTER);
		Label labelPlayer1 = new Label("Player1: " + Globais.getUsernameUtilizadorAtivo() + " - X");
		Label labelPlayer2 = new Label("Player2: " + Globais.getUsernameUtilizadorConvidado() + " - O");
		
		layoutPlayers.getChildren().addAll(labelPlayer1, labelPlayer2);
		
		BorderPane layoutRoot = new BorderPane();
		layoutRoot.setTop(layoutPlayers);
		layoutRoot.setCenter(layout);
		
		
		Scene scene = new Scene(layoutRoot);								//Criar a Scene e associa o Layout
		janelaTicTacToe.setScene(scene);								//Associa a Scena 
		janelaTicTacToe.showAndWait();									//Executa e prende o controlo at� ser fechada
				
	}

	//Confirma se h� ou n�o vencedor a cada jogada
	public static void confirmarVencedor() {		
		//Testa as 3 colunas (vertical) para ver se o jogador x ganhar o jogo
		//Testa na 1�Coluna
		if(label1.getGraphic() == imgv1x && label4.getGraphic() == imgv4x && label7.getGraphic() == imgv7x) {
			Globais.setVencedor(1);
		}
		//Testa na 2�Coluna
		else if (label2.getGraphic() == imgv2x && label5.getGraphic() == imgv5x && label8.getGraphic() == imgv8x) {
			Globais.setVencedor(1);
		}
		//Testa na 3�Coluna
		else if (label3.getGraphic() == imgv3x && label6.getGraphic() == imgv6x && label9.getGraphic() == imgv9x) {
			Globais.setVencedor(1);
		}
		//Testa as 3 linhas (horizontal) para ver se o jogador x ganhar o jogo
		//Testa na 1�Linha
		if(label1.getGraphic() == imgv1x && label2.getGraphic() == imgv2x && label3.getGraphic() == imgv3x) {
			Globais.setVencedor(1);
		}
		//Testa na 2�Linha
		else if (label4.getGraphic() == imgv4x && label5.getGraphic() == imgv5x && label6.getGraphic() == imgv6x) {
			Globais.setVencedor(1);
		}
		//Testa na 3�Linha
		else if (label7.getGraphic() == imgv7x && label8.getGraphic() == imgv8x && label9.getGraphic() == imgv9x) {
			Globais.setVencedor(1);
		}
		//Testa as 2 linhas diagonais para ver se o jogador x ganhar o jogo
		//Testa na 1�Diagonal
		if(label1.getGraphic() == imgv1x && label5.getGraphic() == imgv5x && label9.getGraphic() == imgv9x) {
			Globais.setVencedor(1);
		}
		//Testa na 2�Diagonal
		else if (label3.getGraphic() == imgv3x && label5.getGraphic() == imgv5x && label7.getGraphic() == imgv7x) {
			Globais.setVencedor(1);
		}
				
		//Testa as 3 colunas (vertical) para ver se o jogador o ganhar o jogo
		//Testa na 1�Coluna
		if(label1.getGraphic() == imgv1o && label4.getGraphic() == imgv4o && label7.getGraphic() == imgv7o) {
			Globais.setVencedor(2);
		}
		//Testa na 2�Coluna
		else if (label2.getGraphic() == imgv2o && label5.getGraphic() == imgv5o && label8.getGraphic() == imgv8o) {
			Globais.setVencedor(2);
		}
		//Testa na 3�Coluna
		else if (label3.getGraphic() == imgv3o && label6.getGraphic() == imgv6o && label9.getGraphic() == imgv9o) {
			Globais.setVencedor(2);
		}
		//Testa as 3 linhas (horizontal) para ver se o jogador o ganhar o jogo
		//Testa na 1�Linha
		if(label1.getGraphic() == imgv1o && label2.getGraphic() == imgv2o && label3.getGraphic() == imgv3o) {
			Globais.setVencedor(2);
		}
		//Testa na 2�Linha
		else if (label4.getGraphic() == imgv4o && label5.getGraphic() == imgv5o && label6.getGraphic() == imgv6o) {
			Globais.setVencedor(2);
		}
		//Testa na 3�Linha
		else if (label7.getGraphic() == imgv7o && label8.getGraphic() == imgv8o && label9.getGraphic() == imgv9o) {
			Globais.setVencedor(2);
		}
		//Testa as 2 linhas diagonais para ver se o jogador o ganhar o jogo
		//Testa na 1�Diagonal
		if(label1.getGraphic() == imgv1o && label5.getGraphic() == imgv5o && label9.getGraphic() == imgv9o) {
			Globais.setVencedor(2);
		}
		//Testa na 2�Diagonal
		else if (label3.getGraphic() == imgv3o && label5.getGraphic() == imgv5o && label7.getGraphic() == imgv7o) {
			Globais.setVencedor(2);
		}
		
		//Se h� empate, todas as casas t�m um x ou um o mas n�o h� uma sequ�ncia de 3 casas com o mesmo s�mbolo
		else if(label1.getGraphic() != imgv1 && 
				label2.getGraphic() != imgv2 && 
				label3.getGraphic() != imgv3 && 
				label4.getGraphic() != imgv4 && 
				label5.getGraphic() != imgv5 && 
				label6.getGraphic() != imgv6 && 
				label7.getGraphic() != imgv7 && 
				label8.getGraphic() != imgv8 && 
				label9.getGraphic() != imgv9){
			Globais.setVencedor(3);
		}
	}
	
	//Regista o jogo depois de ter acabo
	public static void registarJogo() {
		
		//Recebe a data e hora do sistema com o formato pretendido
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		
		//Verifica o vencedor
		//Se o jogador x ganhou
		if(Globais.getVencedor() == 1) {
			//Cria o registo do jogo
			
			
			//System.out.println(dateFormat.format(date));
			
			// Executar o DML Insert, com os dados no formato correto
			// Lembrar que o m�todo Liga ao SGBD e abre a BD tictactoe
			// S� � preciso passar o comando DML (insert, update, delete)
			//O c�digo do jogador faz automaticamente a itera��o
			//System.out.println(Globais.getCodUtilizadorAtivo() + " " + Globais.getCodUtilizadorConvidado() + " " + Globais.getUsernameUtilizadorAtivo() + " " + Globais.getUsernameUtilizadorConvidado() + " " + Globais.getPasswordUtilizadorAtivo() + " " + Globais.getPasswordUtilizadorConvidado());
			UtilsSQLConn.mySqlDml("Insert into jogar"				// Tabela jogar
					+" (Data, Carimbo)"	// Nomes Colunas
					+" Values('"+dateFormat.format(date)+"', "+false+")");	// Dados
			/*NOTAS:
			 * 1 - os campos alfab�ticos t�m que ser envolvidos em plicas
			 * 2 - os campos num�ricos n�o t�m.
			 * */
			
			//Globais.listaJogos.add(new Jogo(Globais.listaJogos.size() + 1, Globais.getCodUtilizadorAtivo(), Globais.getCodUtilizadorConvidado(), Globais.getUsernameUtilizadorAtivo(), Globais.getUsernameUtilizadorConvidado(), Globais.getPasswordUtilizadorAtivo(), Globais.getPasswordUtilizadorConvidado(), "Player1 Ganhou"));
			//alertBox("Match", "Player1 Ganhou!");
			
			//Player1
			/*						
			UtilsSQLConn.mySqlDml("Update Jogador set"
					+ " NumVitorias 	= "+Globais.listaJogadores.get(i).getNumVitorias()+" "			// aspas e plicas em strings
					+ " where codJogador = "+Globais.getCodUtilizadorAtivo()+" ");
			*/
			/*
			for(i = 0; i < UtilsSQLConn.carregaListaJogadores().size(); i++) {
				if(UtilsSQLConn.carregaListaJogadores().get(i).getUsername().equals(textFieldUserName.getText()) &&
				   UtilsSQLConn.carregaListaJogadores().get(i).getPassword().equals(passwordFieldPassword.getText())) {
					Globais.setCodUtilizadorConvidado(UtilsSQLConn.carregaListaJogadores().get(i).getCodJogador());
					Globais.setUsernameUtilizadorConvidado(UtilsSQLConn.carregaListaJogadores().get(i).getUsername());
					Globais.setPasswordUtilizadorConvidado(UtilsSQLConn.carregaListaJogadores().get(i).getPassword());
					loginDoConvidado = true;
				}
			}
			*/
			
			//Estrutura de controlo para inserir a vit�ria na conta do utilizador ativo caso seja jogador
			for(i = 0; i < UtilsSQLConn.carregaListaJogadores().size(); i++) {
				/*if(Globais.listaJogadores.get(i).getCodJogador() == Globais.getCodUtilizadorAtivo() &&
				   Globais.listaJogadores.get(i).getUsername().equals(Globais.getUsernameUtilizadorAtivo())) {
					Globais.listaJogadores.get(i).setNumVitorias(Globais.listaJogadores.get(i).getNumVitorias() + 1);
				}*/
				
				
				if(UtilsSQLConn.carregaListaJogadores().get(i).getCodJogador() == Globais.getCodUtilizadorAtivo() &&
					UtilsSQLConn.carregaListaJogadores().get(i).getUsername().equals(Globais.getUsernameUtilizadorAtivo())) {
					try {
						UtilsSQLConn.carregaListaJogadores().get(i).setNumVitorias(UtilsSQLConn.carregaListaJogadores().get(i).getNumVitorias() + 1);
						int valor1 = UtilsSQLConn.carregaListaJogadores().get(i).getNumVitorias() + 1;
						UtilsSQLConn.mySqlDml("Update Jogador set"
								+ " NumVitorias 	= "+valor1+" "			// aspas e plicas em strings
								+ " where codJogador = "+Globais.getCodUtilizadorAtivo()+" ");
						
						UtilsSQLConn.mySqlDml("Insert into jogador_jogar"					// Tabela Jogador
								+" (JogarCodJogo, JogadorCodJogador, Username, Resultado)"		// Nomes Colunas
								+" Values("+UtilsSQLConn.carregaListaJogos().size()+", "+Globais.getCodUtilizadorAtivo()+", '"+Globais.getUsernameUtilizadorAtivo()+"', '"+"Ganhou"+"')");	// Dados
					}
					catch(Exception ex) {
						alertBox("Excep��o", ex.getMessage());
					}
				}
			}
			
			//Player2
			
			//Estrutura de controlo para inserir a derrota na conta do utilizador convidado caso seja jogador
			for(i = 0; i < UtilsSQLConn.carregaListaJogadores().size(); i++) {
				/*if(Globais.listaJogadores.get(i).getCodJogador() == Globais.getCodUtilizadorConvidado() &&
				   Globais.listaJogadores.get(i).getUsername().equals(Globais.getUsernameUtilizadorConvidado())) {
					Globais.listaJogadores.get(i).setNumDerrotas(Globais.listaJogadores.get(i).getNumDerrotas() + 1);
				}*/
				
				if(UtilsSQLConn.carregaListaJogadores().get(i).getCodJogador() == Globais.getCodUtilizadorConvidado() &&
					UtilsSQLConn.carregaListaJogadores().get(i).getUsername().equals(Globais.getUsernameUtilizadorConvidado())) {
					try {
						UtilsSQLConn.carregaListaJogadores().get(i).setNumDerrotas(UtilsSQLConn.carregaListaJogadores().get(i).getNumDerrotas() + 1);
						int valor2 = UtilsSQLConn.carregaListaJogadores().get(i).getNumDerrotas() + 1;
						UtilsSQLConn.mySqlDml("Update Jogador set"
								+ " NumDerrotas 	= "+valor2+" "			// aspas e plicas em strings
								+ " where codJogador = "+Globais.getCodUtilizadorConvidado()+" ");
						
						UtilsSQLConn.mySqlDml("Insert into jogador_jogar"					// Tabela Jogador
								+" (JogarCodJogo, JogadorCodJogador, Username, Resultado)"		// Nomes Colunas
								+" Values("+UtilsSQLConn.carregaListaJogos().size()+", "+Globais.getCodUtilizadorConvidado()+", '"+Globais.getUsernameUtilizadorConvidado()+"', '"+"Perdeu"+"')");	// Dados
					}
					catch(Exception ex) {
						alertBox("Excep��o", ex.getMessage());
					}
					
				}
				
			}
			
		}
		//Se o jogador o ganhou
		else if(Globais.getVencedor() == 2) {
			//Cria o registo do jogo
			//O c�digo do jogador faz automaticamente a itera��o
			UtilsSQLConn.mySqlDml("Insert into jogar"				// Tabela jogar
					+" (Data, Carimbo)"	// Nomes Colunas
					+" Values('"+dateFormat.format(date)+"', "+false+")");	// Dados
			/*NOTAS:
			 * 1 - os campos alfab�ticos t�m que ser envolvidos em plicas
			 * 2 - os campos num�ricos n�o t�m.
			 * */
			
			//Globais.listaJogos.add(new Jogo(Globais.listaJogos.size() + 1, Globais.getCodUtilizadorAtivo(), Globais.getCodUtilizadorConvidado(), Globais.getUsernameUtilizadorAtivo(), Globais.getUsernameUtilizadorConvidado(), Globais.getPasswordUtilizadorAtivo(), Globais.getPasswordUtilizadorConvidado(), "Player2 Ganhou"));
			//alertBox("Match", "Player2 Ganhou!");
			
			//Player1
			
			//Estrutura de controlo para inserir a derrota na conta do utilizador ativo caso seja jogador
			for(i = 0; i < UtilsSQLConn.carregaListaJogadores().size(); i++) {
				/*if(Globais.listaJogadores.get(i).getCodJogador() == Globais.getCodUtilizadorAtivo() &&
				   Globais.listaJogadores.get(i).getUsername().equals(Globais.getUsernameUtilizadorAtivo())) {
					Globais.listaJogadores.get(i).setNumDerrotas(Globais.listaJogadores.get(i).getNumDerrotas() + 1);
				}*/

				if(UtilsSQLConn.carregaListaJogadores().get(i).getCodJogador() == Globais.getCodUtilizadorAtivo() &&
					UtilsSQLConn.carregaListaJogadores().get(i).getUsername().equals(Globais.getUsernameUtilizadorAtivo())) {
					try {
						UtilsSQLConn.carregaListaJogadores().get(i).setNumDerrotas(UtilsSQLConn.carregaListaJogadores().get(i).getNumDerrotas() + 1);
						int valor1 = UtilsSQLConn.carregaListaJogadores().get(i).getNumDerrotas() + 1;
						UtilsSQLConn.mySqlDml("Update Jogador set"
								+ " NumDerrotas 	= "+valor1+" "			// aspas e plicas em strings
								+ " where codJogador = "+Globais.getCodUtilizadorAtivo()+" ");

						UtilsSQLConn.mySqlDml("Insert into jogador_jogar"					// Tabela Jogador
								+" (JogarCodJogo, JogadorCodJogador, Username, Resultado)"		// Nomes Colunas
								+" Values("+UtilsSQLConn.carregaListaJogos().size()+", "+Globais.getCodUtilizadorAtivo()+", '"+Globais.getUsernameUtilizadorAtivo()+"', '"+"Perdeu"+"')");	// Dados
					}
					catch(Exception ex) {
						alertBox("Excep��o", ex.getMessage());
					}
					
				}
			}
			
			//Player2
			
			//Estrutura de controlo para inserir a vit�ria na conta do utilizador convidado caso seja jogador
			for(i = 0; i < UtilsSQLConn.carregaListaJogadores().size(); i++) {
				/*if(Globais.listaJogadores.get(i).getCodJogador() == Globais.getCodUtilizadorConvidado() &&
				   Globais.listaJogadores.get(i).getUsername().equals(Globais.getUsernameUtilizadorConvidado())) {
					Globais.listaJogadores.get(i).setNumVitorias(Globais.listaJogadores.get(i).getNumVitorias() + 1);
				}*/

				if(UtilsSQLConn.carregaListaJogadores().get(i).getCodJogador() == Globais.getCodUtilizadorConvidado() &&
					UtilsSQLConn.carregaListaJogadores().get(i).getUsername().equals(Globais.getUsernameUtilizadorConvidado())) {
					try {
						UtilsSQLConn.carregaListaJogadores().get(i).setNumVitorias(UtilsSQLConn.carregaListaJogadores().get(i).getNumVitorias() + 1);
						int valor2 = UtilsSQLConn.carregaListaJogadores().get(i).getNumVitorias() + 1;
						UtilsSQLConn.mySqlDml("Update Jogador set"
								+ " NumVitorias 	= "+valor2+" "			// aspas e plicas em strings
								+ " where codJogador = "+Globais.getCodUtilizadorConvidado()+" ");
						
						UtilsSQLConn.mySqlDml("Insert into jogador_jogar"					// Tabela Jogador
								+" (JogarCodJogo, JogadorCodJogador, Username, Resultado)"		// Nomes Colunas
								+" Values("+UtilsSQLConn.carregaListaJogos().size()+", "+Globais.getCodUtilizadorConvidado()+", '"+Globais.getUsernameUtilizadorConvidado()+"', '"+"Ganhou"+"')");	// Dados
					}
					catch(Exception ex) {
						alertBox("Excep��o", ex.getMessage());
					}
					
				}
			}
		}
		//Se houve empate
		else if(Globais.getVencedor() == 3) {
			//Cria o registo do jogo
			//O c�digo do jogador faz automaticamente a itera��o
			UtilsSQLConn.mySqlDml("Insert into jogar"				// Tabela jogar
					+" (Data, Carimbo)"	// Nomes Colunas
					+" Values('"+dateFormat.format(date)+"', "+false+")");	// Dados
			/*NOTAS:
			 * 1 - os campos alfab�ticos t�m que ser envolvidos em plicas
			 * 2 - os campos num�ricos n�o t�m.
			 * */
			
			//Globais.listaJogos.add(new Jogo(Globais.listaJogos.size() + 1, Globais.getCodUtilizadorAtivo(), Globais.getCodUtilizadorConvidado(), Globais.getUsernameUtilizadorAtivo(), Globais.getUsernameUtilizadorConvidado(), Globais.getPasswordUtilizadorAtivo(), Globais.getPasswordUtilizadorConvidado(), "Empate"));
			//alertBox("Match", "Empate!");
			
			//Player1
			
			//Estrutura de controlo para inserir o empate na conta do utilizador ativo caso seja jogador
			for(i = 0; i < UtilsSQLConn.carregaListaJogadores().size(); i++) {
				/*if(Globais.listaJogadores.get(i).getCodJogador() == Globais.getCodUtilizadorAtivo() &&
				   Globais.listaJogadores.get(i).getUsername().equals(Globais.getUsernameUtilizadorAtivo())) {
					Globais.listaJogadores.get(i).setNumEmpates(Globais.listaJogadores.get(i).getNumEmpates() + 1);
				}*/
				
				if(UtilsSQLConn.carregaListaJogadores().get(i).getCodJogador() == Globais.getCodUtilizadorAtivo() &&
					UtilsSQLConn.carregaListaJogadores().get(i).getUsername().equals(Globais.getUsernameUtilizadorAtivo())) {
					try {
						UtilsSQLConn.carregaListaJogadores().get(i).setNumEmpates(UtilsSQLConn.carregaListaJogadores().get(i).getNumEmpates() + 1);
						int valor1 = UtilsSQLConn.carregaListaJogadores().get(i).getNumEmpates() + 1;
						UtilsSQLConn.mySqlDml("Update Jogador set"
								+ " NumEmpates 	= "+valor1+" "			// aspas e plicas em strings
								+ " where codJogador = "+Globais.getCodUtilizadorAtivo()+" ");

						UtilsSQLConn.mySqlDml("Insert into jogador_jogar"					// Tabela Jogador
								+" (JogarCodJogo, JogadorCodJogador, Username, Resultado)"		// Nomes Colunas
								+" Values("+UtilsSQLConn.carregaListaJogos().size()+", "+Globais.getCodUtilizadorAtivo()+", '"+Globais.getUsernameUtilizadorAtivo()+"', '"+"Empatou"+"')");	// Dados
					}
					catch(Exception ex) {
						alertBox("Excep��o", ex.getMessage());
					}
					
				}
			}
			
			//Player2
			
			//Estrutura de controlo para inserir o empate na conta do utilizador convidado caso seja jogador
			for(i = 0; i < UtilsSQLConn.carregaListaJogadores().size(); i++) {
				/*if(Globais.listaJogadores.get(i).getCodJogador() == Globais.getCodUtilizadorConvidado() &&
				   Globais.listaJogadores.get(i).getUsername().equals(Globais.getUsernameUtilizadorConvidado())) {
					Globais.listaJogadores.get(i).setNumEmpates(Globais.listaJogadores.get(i).getNumEmpates() + 1);
				}*/
				
				if(UtilsSQLConn.carregaListaJogadores().get(i).getCodJogador() == Globais.getCodUtilizadorConvidado() &&
					UtilsSQLConn.carregaListaJogadores().get(i).getUsername().equals(Globais.getUsernameUtilizadorConvidado())) {
					try {
						UtilsSQLConn.carregaListaJogadores().get(i).setNumEmpates(UtilsSQLConn.carregaListaJogadores().get(i).getNumEmpates() + 1);
						int valor2 = UtilsSQLConn.carregaListaJogadores().get(i).getNumEmpates() + 1;
						UtilsSQLConn.mySqlDml("Update Jogador set"
								+ " NumEmpates 	= "+valor2+" "			// aspas e plicas em strings
								+ " where codJogador = "+Globais.getCodUtilizadorConvidado()+" ");

						UtilsSQLConn.mySqlDml("Insert into jogador_jogar"					// Tabela Jogador
								+" (JogarCodJogo, JogadorCodJogador, Username, Resultado)"		// Nomes Colunas
								+" Values("+UtilsSQLConn.carregaListaJogos().size()+", "+Globais.getCodUtilizadorConvidado()+", '"+Globais.getUsernameUtilizadorConvidado()+"', '"+"Empatou"+"')");	// Dados
					}
					catch(Exception ex) {
						alertBox("Excep��o", ex.getMessage());
					}
					
				}
			}
		}
		
		Globais.setVencedor(0);			//D� reset ao vencedor
		
		Globais.setJogadorAtivo(0);		//D� reset ao jogador activo
	}
	
	//Janela que apresenta a cria��o de uma conta de jogador
	public static void criarContaJogador(){ //Static para n�o ser instanciada
		//Janela criar conta
		Stage janelaCriacaoContaJogadorNoLogin = new Stage();							//Cria uma window
		//janelaLogin.initModality(Modality.APPLICATION_MODAL);							//Define uma janelaLogin Modal
		janelaCriacaoContaJogadorNoLogin.initModality(Modality.APPLICATION_MODAL);		//Define uma janelaLogin Modal
		janelaCriacaoContaJogadorNoLogin.setTitle("Criar Conta"); 						//Como t�tulo, recebe a string do parametro
		janelaCriacaoContaJogadorNoLogin.setMinWidth(500);								//Largura da janelaLogin
		janelaCriacaoContaJogadorNoLogin.setMaxWidth(500);
		janelaCriacaoContaJogadorNoLogin.setMinHeight(300);
		janelaCriacaoContaJogadorNoLogin.setMaxHeight(300);
		
		//layout 
		GridPane layout = new GridPane();
		//Labels
		Label labelUserName = new Label("Username: ");
		Label labelPassword = new Label("Password: ");
		//TextFields
		TextField textFieldUserName = new TextField();
		PasswordField passwordFieldPassword = new PasswordField();
		
		Button btnOk = new Button("OK");							//Cria bot�o para fazer login
		btnOk.setOnAction(e -> {
			
			//D� reset ao valor de verifica��o da cria��o de conta
			criacaoNegada = false;
			
			if(textFieldUserName.getText().equals("") || passwordFieldPassword.getText().equals("")) {
				alertBox("Erro", "Um ou mais campos nulos!");
				criacaoNegada = true;
			}
			else {
				//Estrutura de controlo para verificar se o username e password est�o na lista de jogadores
				for(i = 0; i < Globais.listaJogadores.size(); i++) {
					if(UtilsSQLConn.carregaListaJogadores().get(i).getUsername().equals(textFieldUserName.getText())) {
						criacaoNegada = true;
					}
				}
				
				if(criacaoNegada == false) {
					
					// Executar o DML Insert, com os dados no formato correto
					// Lembrar que o m�todo Liga ao SGBD e abre a BD tictactoe
					// S� � preciso passar o comando DML (insert, update, delete)
					
					//O c�digo do jogador faz automaticamente a itera��o
					UtilsSQLConn.mySqlDml("Insert into jogador"					// Tabela Jogador
							+" (Username, Password, NumVitorias, NumDerrotas, NumEmpates, Carimbo)"		// Nomes Colunas
							+" Values('"+textFieldUserName.getText()+"', '"+passwordFieldPassword.getText()+"', "+0+", "+0+", "+0+", "+false+")");	// Dados
					/*NOTAS:
					 * 1 - os campos alfab�ticos t�m que ser envolvidos em plicas
					 * 2 - os campos num�ricos n�o t�m.
					 * */
					
					//Globais.listaJogadores.add(new Jogador(Globais.listaJogadores.size() + 1, textFieldUserName.getText(), passwordFieldPassword.getText(), 0, 0, 0));
				}
				else {
					alertBox("Erro", "Conta de utilizador n�o dispon�vel!");
				}
			}			
			
			janelaCriacaoContaJogadorNoLogin.close();
			
		});
		//Adicionar � Grid os botoes, label e textfields-----------------------------
		layout.add(labelUserName, 0, 0);
		layout.add(textFieldUserName, 1, 0);
		layout.add(labelPassword, 0, 2);
		layout.add(passwordFieldPassword, 1,2);
		layout.add(btnOk, 0, 3);
		layout.setAlignment(Pos.CENTER);
		
		//SCENE
		Scene scene = new Scene(layout);						//Criar a Scene e associa o Layout
		janelaCriacaoContaJogadorNoLogin.setScene(scene);		//Associa a Scena 
		janelaCriacaoContaJogadorNoLogin.showAndWait();			//Executa e prende o controlo at� ser fechada
	}
}